## Follow the below checklist to ensure all steps are covered for a release milestone of aevaSlicer.
Use strikethrough (~~like this~~) for items that are not-applicable.\
Use checkboxes for items applicable and completed.

_Based on the state of the relevant sub-tasks, consider updating the issue description changing `:question:` (:question:)  into either `:hourglass:` (:hourglass:) or `:heavy_check_mark:` (:heavy_check_mark:)._

# CheckList:
 - [ ] Merge all forked repositories to their official main branches and/or ensure fork are available in `GitHub/KitwareMedical`.
  * The Slicer repository associated with `FetchContent_Populate` options `GIT_REPOSITORY` and `GIT_TAG` variables in top-level [CMakeLists.txt](https://gitlab.kitware.com/aeva/aevaslicer/-/blob/master/CMakeLists.txt) should correspond to a branch published at https://github.com/KitwareMedical/Slicer
  * External projects are listed in [SuperBuild.cmake](https://gitlab.kitware.com/aeva/aevaslicer/-/blob/master/SuperBuild.cmake), and `SuperBuild/External_*.cmake` files.
  * Consider also reviewing dependencies of bundled Slicer extensions (e.g `SlicerSMTK`)
 - [ ] If not possible to merge within the milestone time frame, create remaining forks in `Github/KitwareMedical` rather than private or user namespaces.
 - [ ] Increment the version number of aevaSlicer in the [slicer-application-properties.cmake](https://gitlab.kitware.com/aeva/aevaslicer/-/blob/master/Applications/AevaSlicerApp/slicer-application-properties.cmake) file.
 - [ ] Update dashboard scripts if necessary. See [MedicalTeamDashboardScripts](https://kwgitlab.kitware.com/dashboardscripts/MedicalTeamDashboardScripts/-/tree/master/AEVA) for aeva-specific scripts. This repository is accessible only through Kitware internal network.
 - [ ] ~~Ensure that all test cases in aevaSlicer and SlicerSMTK extension pass successfully.~~ See [this issue](https://gitlab.kitware.com/aeva/aeva/-/issues/299).
 - [ ] Create Linux, Windows, and macOS packages using the release scripts on the factory machines. Follow the instructions given in the [aevaSlicer build, package, and release process](https://docs.google.com/document/d/174hK_0vtSVWtgog0-oovVhOCNymuGZHIsPEqY4PQZ4g/edit?usp=sharing) document.
 - [ ] Confirm that packages generated using the corresponding hash can be installed. No missing lib errors. \
Linux package ✔️ \
macOS package ✔️ \
- [ ] Confirm that packages generated using the corresponding hash can be installed (e.g No missing library errors).
  * Linux package :question:
  * macOS package :question:
  * Windows package :question:
- [ ] Apply digital signatures to packages.
- [ ] Upload packages to [*data.kitware.com*](https://data.kitware.com/#collection/5e387f9caf2e2eed3562242e/folder/5e7a4690af2e2eed356a17f2) website.
- [ ] Update [README](https://gitlab.kitware.com/aeva/aevaslicer/-/blob/master/README.md) and [Build-Instructions](https://gitlab.kitware.com/aeva/aevaslicer/-/blob/master/BUILD.md) if the build process / requirements / dependencies have changed.

### Create release tag in gitlab with corresponding version numbers for:
- [ ] aevaSlicer
- [ ] SlicerSMTK (if changes were introduced since the last release)
- [ ] Other extensions (if changes were introduced since the last release)

### Update release process if necessary:
- [ ] Changes to this document.
- [ ] Changes to package / release [google-document](https://docs.google.com/document/d/174hK_0vtSVWtgog0-oovVhOCNymuGZHIsPEqY4PQZ4g/edit?usp=sharing).
- [ ] Lastly, create entries in [gitlab issue tracker](https://gitlab.kitware.com/groups/aeva/-/issues) for all pending tasks related to this release.

/label ~"software process"
