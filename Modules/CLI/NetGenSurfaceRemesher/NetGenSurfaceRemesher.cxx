#include "vtkCleanPolyData.h"
#include "vtkNew.h"
#include "vtkTriangleFilter.h"
#include "vtkUnstructuredGrid.h"
#include "vtkUnstructuredGridWriter.h"
#include "vtkSuperquadricSource.h"
#include "vtkDebugLeaks.h"
// MRML includes
#include "vtkMRMLModelNode.h"
#include "vtkMRMLModelStorageNode.h"

#include "NetGenSurfaceRemesherCLP.h"

//STL includes
#include <array>

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4067) /* unexpected tokens following preprocessor directive */
#endif
namespace nglib
{
#include "nglib.h"
}
#ifdef _MSC_VER
#pragma warning(pop)
#endif

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//




int main( int argc, char * argv[] )
{

  PARSE_ARGS;
  int status = 0;
  vtkNew<vtkMRMLModelStorageNode> model1StorageNode;
  model1StorageNode->SetFileName(inputModel.c_str());
  vtkNew<vtkMRMLModelNode> model1Node;
  if (!model1StorageNode->ReadData(model1Node))
  {
    std::cerr << "Failed to read input model 1 file " << inputModel << std::endl;
    return EXIT_FAILURE;
  }

  vtkNew<vtkPolyData> outputPD;
  vtkSmartPointer<vtkPolyData> inputPD = model1Node->GetPolyData();
  nglib::Ng_Init();
  try
  {
    auto* mesh = nglib::Ng_NewMesh();
    auto* stl_geom = nglib::Ng_STL_NewGeometry();
    nglib::Ng_Result ng_res;
    nglib::Ng_Meshing_Parameters mp;
    mp.maxh = maxGlobalMeshSize;
    mp.minh = minGlobalMeshSize;
    mp.fineness = meshDensity;
    mp.grading = meshGrading;
    mp.optsteps_2d = numOptimizeSteps;

    vtkIdType numberOfPoints;
#if VTK_MAJOR_VERSION >= 9 || (VTK_MAJOR_VERSION >= 8 && VTK_MINOR_VERSION >= 90)
    const vtkIdType* connectivity;
#else
    vtkIdType* connectivity;
#endif
    vtkVector3d coords;

    // Add surface triangles
    vtkSmartPointer<vtkCellArray> polys = inputPD->GetPolys();
    vtkSmartPointer<vtkPoints> points = inputPD->GetPoints();
    for (polys->InitTraversal(); polys->GetNextCell(numberOfPoints, connectivity);)
    {
      if (numberOfPoints != 3)
      {
        std::cerr << "Non-triangular surface element detected." << std::endl;
        return EXIT_FAILURE;
      }
      double p1[3];
      double p2[3];
      double p3[3];
      points->GetPoint(connectivity[0], p1);
      points->GetPoint(connectivity[1], p2);
      points->GetPoint(connectivity[2], p3);

      nglib::Ng_STL_AddTriangle(stl_geom, p1, p2, p3);
    }

    ng_res = nglib::Ng_STL_InitSTLGeometry(stl_geom);
    if (ng_res != nglib::NG_OK)
    {
      std::cerr << "Error Initialising the STL Geometry....Aborting!!" << std::endl;
      return EXIT_FAILURE;
    }

    ng_res = nglib::Ng_STL_MakeEdges(stl_geom, mesh, &mp);
    if (ng_res != nglib::NG_OK)
    {
      std::cerr << "Error in Edge Meshing....Aborting!!" << std::endl;
      return EXIT_FAILURE;
    }

    ng_res = nglib::Ng_STL_GenerateSurfaceMesh(stl_geom, mesh, &mp);
    if (ng_res != nglib::NG_OK)
    {
      std::cerr << "Error in Surface Meshing....Aborting!!" << std::endl;
      return EXIT_FAILURE;
    }

    points = outputPD->GetPoints();
    if (!points)
    {
      points = vtkSmartPointer<vtkPoints>::New();
      outputPD->SetPoints(points);
    }
    numberOfPoints = nglib::Ng_GetNP(mesh);
    points->SetNumberOfPoints(numberOfPoints);
    for (int pp = 0; pp < numberOfPoints; ++pp)
    {
      nglib::Ng_GetPoint(mesh, pp + 1, coords.GetData());
      points->SetPoint(pp, coords.GetData());
    }

    std::array<int, 8> ngconn; // Current max number of points per surface element is 8
    std::array<vtkIdType, 8> vtkconn;

    int cellType;
    int nconn;
    int numberOfElements = nglib::Ng_GetNSE(mesh);
    outputPD->Allocate(numberOfElements);

    for (int se = 0; se < numberOfElements; ++se)
    {
      auto elementType = nglib::Ng_GetSurfaceElement(mesh, se + 1, ngconn.data());
      switch (elementType)
      {
      case nglib::NG_TRIG:
        nconn = 3;
        cellType = VTK_TRIANGLE;
        break;
      case nglib::NG_QUAD:
        nconn = 4;
        cellType = VTK_QUAD;
        break;
      case nglib::NG_TRIG6:
        nconn = 6;
        cellType = VTK_QUADRATIC_TRIANGLE;
        break;
      case nglib::NG_QUAD6:
        nconn = 8;
        cellType = VTK_QUADRATIC_QUAD;
        break;
      default:
        nconn = 0;
        cellType = VTK_EMPTY_CELL;
        std::cerr << "Unknown output surface element type (" << elementType << ")." << std::endl;
        break;
      }
      for (int ii = 0; ii < nconn; ++ii)
      {
        vtkconn[ii] = static_cast<vtkIdType>(ngconn[ii] - 1);
      }
      outputPD->InsertNextCell(cellType, nconn, vtkconn.data());
    }
  }
  catch (...)
  {
    std::cerr << "Netgen surface remeshing failed." << std::endl;
    return EXIT_FAILURE;
  }
  nglib::Ng_Exit();
  vtkNew<vtkMRMLModelNode> outputModelNode;
  outputModelNode->SetAndObservePolyData(outputPD);
  vtkNew<vtkMRMLModelStorageNode> outputModelStorageNode;
  outputModelStorageNode->SetFileName(outputModel.c_str());
  if (!outputModelStorageNode->WriteData(outputModelNode))
  {
    std::cerr << "Failed to write output model file " << std::endl;
    return EXIT_FAILURE;
  }



  return EXIT_SUCCESS;
}
