#include <vtkPolyDataWriter.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkPolyDataReader.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkCommand.h>
#include <vtkPolyDataNormals.h>
#include <vtkExtractSurface.h>
#include <vtkPointData.h>
#include <vtkSignedDistance.h>


#include "itkPluginUtilities.h"

#include "ReconstructSurfaceCLP.h"

// Use an anonymous namespace to keep class types and function names
// from colliding when module is used as shared object module.  Every
// thing should be in an anonymous namespace except for the module
// entry point, e.g. main()
//
namespace
{

  //class ErrorObserver copied from http://www.vtk.org/Wiki/VTK/Examples/Cxx/Utilities/ObserveError
  class ErrorObserver : public vtkCommand
  {
  public:
    ErrorObserver() :
      Error(false),
      Warning(false),
      ErrorMessage(""),
      WarningMessage("") {}
    static ErrorObserver *New()
    {
      return new ErrorObserver;
    }
    bool GetError() const
    {
      return this->Error;
    }
    bool GetWarning() const
    {
      return this->Warning;
    }
    void Clear()
    {
      this->Error = false;
      this->Warning = false;
      this->ErrorMessage = "";
      this->WarningMessage = "";
    }
    virtual void Execute(vtkObject *vtkNotUsed(caller),
      unsigned long event,
      void *calldata)
    {
      switch (event)
      {
      case vtkCommand::ErrorEvent:
        ErrorMessage = static_cast<char *>(calldata);
        this->Error = true;
        break;
      case vtkCommand::WarningEvent:
        WarningMessage = static_cast<char *>(calldata);
        this->Warning = true;
        break;
      }
    }
    std::string GetErrorMessage()
    {
      return ErrorMessage;
    }
    std::string GetWarningMessage()
    {
      return WarningMessage;
    }
  private:
    bool        Error;
    bool        Warning;
    std::string ErrorMessage;
    std::string WarningMessage;
  };


int ReadVTK( std::string input , vtkSmartPointer<vtkPolyData> &polyData )
{
    vtkSmartPointer<ErrorObserver>  errorObserver =
            vtkSmartPointer<ErrorObserver>::New();
    if( input.rfind( ".vtk" ) != std::string::npos )
    {
      vtkSmartPointer< vtkPolyDataReader > polyReader = vtkSmartPointer<vtkPolyDataReader>::New();
        polyReader->AddObserver( vtkCommand::ErrorEvent , errorObserver ) ;
        polyReader->SetFileName( input.c_str() ) ;
        polyData = polyReader->GetOutput() ;
        polyReader->Update() ;
    }
    else if( input.rfind( ".vtp" ) != std::string::npos )
    {
      vtkSmartPointer< vtkXMLPolyDataReader > xmlReader = vtkSmartPointer<vtkXMLPolyDataReader>::New();
        xmlReader->SetFileName( input.c_str() ) ;
        xmlReader->AddObserver( vtkCommand::ErrorEvent , errorObserver ) ;
        polyData = xmlReader->GetOutput() ;
        xmlReader->Update() ;
    }
    else
    {
        std::cerr << "Input file format not handled: " << input << " cannot be read" << std::endl ;
        return 1 ;
    }
    if (errorObserver->GetError())
    {
        std::cout << "Caught error opening " << input << std::endl ;
        return 1 ;
    }
    return 0 ;
}

int WriteVTK( std::string output , vtkSmartPointer<vtkPolyData> &polyData )
{
    vtkSmartPointer<ErrorObserver>  errorObserver =
            vtkSmartPointer<ErrorObserver>::New();
    if (output.rfind(".vtk") != std::string::npos )
    {
        vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New() ;
        writer->SetFileName( output.c_str() ) ;
        writer->AddObserver( vtkCommand::ErrorEvent , errorObserver ) ;
        writer->SetInputData( polyData ) ;
        writer->Update();
    }
    else if( output.rfind( ".vtp" ) != std::string::npos )
    {
      vtkSmartPointer< vtkXMLPolyDataWriter > writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
        writer->SetFileName( output.c_str() ) ;
        writer->AddObserver( vtkCommand::ErrorEvent , errorObserver ) ;
        writer->SetInputData( polyData ) ;
        writer->Update() ;
    }
    else
    {
        std::cerr << "Output file format not handled: " << output << " cannot be written" << std::endl ;
        return 1 ;
    }
    if (errorObserver->GetError())
    {
        std::cout << "Caught error saving " << output << std::endl ;
        return 1 ;
    }
    return 0 ;
}


} // end of anonymous namespace

int main( int argc, char * argv[] )
{
  PARSE_ARGS;
  vtkSmartPointer<vtkPolyData> inputPD = vtkSmartPointer<vtkPolyData>::New();
  if (ReadVTK(inputModel, inputPD))
  {
    return 1;
  }

  // If the geometric data does not have point normals...
  if (!inputPD->GetPointData()->GetNormals())
  {
    //...compute them.
    vtkNew<vtkPolyDataNormals> genNormals;
    genNormals->SetInputDataObject(inputPD);
    genNormals->ConsistencyOn();
    genNormals->SplittingOff();
    genNormals->Update();
    inputPD = genNormals->GetOutput();
  }

  //Bounding box
  std::array<double, 6> boundingBox;
  inputPD->GetBounds(boundingBox.data());

  std::array<int, 3> dims = {    dimensions[0], dimensions[1], dimensions[2]  };


  // Extend the bounding box by 5% along all axes to avoid clipping
  for (int i = 0; i < 3; i++)
  {
    double length = boundingBox[2 * i + 1] - boundingBox[2 * i];
    boundingBox[2 * i] -= length * .05;
    boundingBox[2 * i + 1] += length * .05;
  }

  // Compute an implicit signed distance image data around the surface
  vtkNew<vtkSignedDistance> signedDistance;
  signedDistance->SetDimensions(dimensions.data());
  signedDistance->SetBounds(boundingBox.data());
  signedDistance->SetRadius(radius);
  signedDistance->SetInputDataObject(inputPD);

  // Using the implicit signed distance image data, extract the surface
  vtkNew<vtkExtractSurface> extractSurface;
  extractSurface->SetInputConnection(signedDistance->GetOutputPort());
  extractSurface->SetRadius(radius);
  extractSurface->SetComputeNormals(false);
  if (fillHoles)
  {
    extractSurface->HoleFillingOn();
  }
  else
  {
    extractSurface->HoleFillingOff();
  }
  extractSurface->Update();

  vtkSmartPointer<vtkPolyData>  outputPD = extractSurface->GetOutput();
  return WriteVTK(outputModel, outputPD);

  
  return EXIT_SUCCESS;
}
