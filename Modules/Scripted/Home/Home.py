import os
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import logging
from slicer.util import VTKObservationMixin
import textwrap

#
# Home
#

class Home(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "Home" # TODO make this more human readable by adding spaces
    self.parent.categories = [""]
    self.parent.dependencies = ["SegmentEditor", "DICOM", "Data", "SurfaceToolbox", "ReconstructSurface", "NetGenSurfaceRemesher", "SegmentMesher"]
    self.parent.contributors = ["Sam Horvath (Kitware Inc.)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
This is the Home module for the AevaSliceer application
"""
    self.parent.helpText += self.getDefaultModuleDocumentationLink()
    self.parent.acknowledgementText = """
This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
""" # replace with organization, grant and thanks.

#
# HomeWidget
#

class HomeWidget(ScriptedLoadableModuleWidget, VTKObservationMixin):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModuleWidget.__init__(self, parent)
    VTKObservationMixin.__init__(self)

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # Load widget from .ui file (created by Qt Designer)
    uiWidget = slicer.util.loadUI(self.resourcePath('UI/Home.ui'))
    self.layout.addWidget(uiWidget)
    self.ui = slicer.util.childWidgetVariables(uiWidget)
    uiWidget.setPalette(slicer.util.mainWindow().style().standardPalette())

    self.modifyWindowUI()

    
    #Collect tab indicies
    self.ui.ModuleTabWidget.currentChanged.connect(self.onTabChanged)
    self.DataTabIndex = self.ui.ModuleTabWidget.indexOf(self.ui.DataTab)
    self.EditorTabIndex = self.ui.ModuleTabWidget.indexOf(self.ui.EditorTab)
    self.MeshingTabIndex = self.ui.ModuleTabWidget.indexOf(self.ui.MeshingTab)
    self.SurfaceTabIndex = self.ui.ModuleTabWidget.indexOf(self.ui.SurfaceTab)
    self.CurrentIndex = -1

    


    #Segment Editor    
    self.segWidget = slicer.modules.segmenteditor.createNewWidgetRepresentation()
    self.ui.EditorTab.layout().addWidget(self.segWidget)
    self.ui.EditorTab.layout().addStretch(1)

    #Surface export
    self.surfaceWidget = slicer.modules.surfacetoolbox.createNewWidgetRepresentation()
    self.reconstructionWidget = slicer.modules.reconstructsurface.createNewWidgetRepresentation()
    self.remeshingWidget = slicer.modules.netgensurfaceremesher.createNewWidgetRepresentation()
    self.ui.ReconstructionTab.layout().addWidget(self.reconstructionWidget)
    self.ui.RemeshingTab.layout().addWidget(self.remeshingWidget)
    self.ui.ToolBoxTab.layout().addWidget(self.surfaceWidget)
    self.setupSurfaceMeshing()
  

    #Meshing
    self.meshWidget = slicer.modules.segmentmesher.createNewWidgetRepresentation()
    self.ui.MeshingTab.layout().addWidget(self.meshWidget)
    self.ui.MeshingTab.layout().addStretch(1)

    #Data Widget
    self.dataWidget = slicer.modules.data.createNewWidgetRepresentation()
    self.dicomButton = qt.QPushButton('Show/Hide DICOM Browser')   
    self.segmentButton = qt.QPushButton('Load labelmaps as segmentation') 
    self.ui.DataTab.layout().addWidget(self.dicomButton)
    self.ui.DataTab.layout().addWidget(self.segmentButton)
    self.ui.DataTab.layout().addWidget(self.dataWidget)
    
    #Initialize tab tracking
    self.ui.ModuleTabWidget.setCurrentIndex(self.DataTabIndex)
    self.onTabChanged(self.DataTabIndex)

    #Data pane
    self.setupDataPane()
    
    #Make sure DICOM widget exists
    slicer.app.connect("startupCompleted()", self.setupDICOMBrowser)
    self.segmentButton.clicked.connect(self.loadLabelmapStack)

    #listen for close event to be able to re setup when needed
    slicer.mrmlScene.AddObserver(slicer.vtkMRMLScene.EndCloseEvent, self.onClose)
    slicer.mrmlScene.AddObserver(slicer.vtkMRMLScene.StartCloseEvent, self.onCloseStart)
    
    #Apply style
    # self.applyStyle()

    self.initialExportDone = False

  def loadLabelmapStack(self):
    directoryPath = str(qt.QFileDialog.getExistingDirectory(self.ui.DataTab, "Select Directory to load labelmaps - directory should contain ONLY labelmaps"))
    name = os.path.basename(os.path.normpath(directoryPath))

    from os import listdir
    from os.path import isfile, join
    filePaths = [f for f in listdir(directoryPath) if isfile(join(directoryPath, f))]
    labelOnly = False

    seg = slicer.mrmlScene.AddNewNodeByClass('vtkMRMLSegmentationNode')
    seg.SetName(name)

    for filePath in filePaths:
      if not "label" in filePath and labelOnly:
        continue
      try:
        fullpath = os.path.join(directoryPath, filePath)
        node = slicer.util.loadLabelVolume(fullpath)
        slicer.modules.segmentations.logic().ImportLabelmapToSegmentationNode(node, seg)
        slicer.mrmlScene.RemoveNode(node)
      except:
        print('Failed to load ' + filePath + ' as labelmap')
  
  def cleanup(self):
    self.removeObservers()
  
  def onCloseStart(self, unusedOne, unusedTwo):
    self.ui.SubjectHierarchyTreeView.setRootItem(-1)
    self.ui.SubjectHierarchyTreeViewOutput.setRootItem(-1)
    

  def onClose(self, unusedOne, unusedTwo):    
    slicer.app.processEvents()
    #self.ui.SubjectHierarchyTreeView.setRootItem(0)
    #self.ui.SubjectHierarchyTreeViewOutput.setRootItem(0)
    self.ui.SubjectHierarchyTreeView.setMRMLScene(None)
    self.ui.SubjectHierarchyTreeViewOutput.setMRMLScene(None)
    self.initialExportDone = False

    
  
  def setupDataPane(self):
    dockWidget = qt.QDockWidget(slicer.util.mainWindow())
    dockWidget.name = 'DataViewDockWidget'
    contents = qt.QSplitter(dockWidget)
    contents.setOrientation(qt.Qt.Vertical)
    dockWidget.setWidget(contents)
    dockWidget.setFeatures(dockWidget.AllDockWidgetFeatures)
    slicer.util.mainWindow().addDockWidget(qt.Qt.RightDockWidgetArea , dockWidget)
    dataView = slicer.qMRMLSubjectHierarchyTreeView()
    dataView.setMRMLScene(slicer.mrmlScene)
    propertiesScroll = qt.QScrollArea()
    self.propertiesView = qt.QStackedWidget()
    self.modelsView = slicer.modules.models.createNewWidgetRepresentation()
    self.volumesView = slicer.modules.volumes.createNewWidgetRepresentation()
    self.segmentsView = slicer.modules.segmentations.createNewWidgetRepresentation()
    self.blankView = qt.QWidget()
    self.propertiesView.addWidget(self.blankView)
    self.propertiesView.addWidget(self.modelsView)

    slicer.util.findChild(self.modelsView, 'SubjectHierarchyTreeView').visible = False
    slicer.util.findChild(self.modelsView, 'hideAllModelsButton').visible = False
    slicer.util.findChild(self.modelsView, 'showAllModelsButton').visible = False
    slicer.util.findChild(self.modelsView, 'FilterModelSearchBox').visible = False
    slicer.util.findChild(self.volumesView, 'ActiveVolumeLabel').visible = False
    slicer.util.findChild(self.volumesView, 'ActiveVolumeNodeSelector').visible = False
    slicer.util.findChild(self.segmentsView, 'label_ActiveSegmentation').visible = False
    slicer.util.findChild(self.segmentsView, 'label_ReferenceVolumeText').visible = False
    slicer.util.findChild(self.segmentsView, 'MRMLNodeComboBox_Segmentation').visible = False
    slicer.util.findChild(self.segmentsView, 'label_ReferenceVolumeName').visible = False
    slicer.util.findChild(self.segmentsView, 'toolButton_Edit').visible = False
    slicer.util.findChild(self.segmentsView, 'show3DButton').visible = False

    self.propertiesView.addWidget(self.volumesView)
    self.propertiesView.addWidget(self.segmentsView)
    propertiesScroll.setWidget(self.propertiesView)
    contents.addWidget(dataView)
    contents.addWidget(propertiesScroll)
    dataView.connect("currentItemChanged(vtkIdType)", self.onDataViewItemChanged)    
    self.propertiesView.show()
    propertiesScroll.show()
    
  def onDataViewItemChanged(self, item):
    shNode = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)
    node =  shNode.GetItemDataNode(item)
    if node is None:
      parent_item = shNode.GetItemParent(item)
      node = shNode.GetItemDataNode(parent_item)
    if(node):
      if node.IsA('vtkMRMLModelNode'):
        self.modelsView.setEditedNode(node)
        self.propertiesView.setCurrentWidget(self.modelsView)

      elif node.IsA('vtkMRMLVolumeNode'):
        self.volumesView.setEditedNode(node)
        self.propertiesView.setCurrentWidget(self.volumesView)
      elif node.IsA('vtkMRMLSegmentationNode'):
        self.segmentsView.setEditedNode(node)
        self.propertiesView.setCurrentWidget(self.segmentsView)
      else:
        self.propertiesView.setCurrentWidget(self.blankView)
    else:
      self.propertiesView.setCurrentWidget(self.blankView)
    

  
  def setupSurfaceMeshing(self):
    self.ui.InputSegmentationComboBox.setMRMLScene(slicer.mrmlScene)   
    self.ui.OutputFolderComboBox.setMRMLScene(slicer.mrmlScene)

    #Set up SubjectHieratchy combo
    nodeTypes = ['vtkMRMLFolderDisplayNode']
    levelFilters = [slicer.vtkMRMLSubjectHierarchyConstants().GetDICOMLevelPatient(), slicer.vtkMRMLSubjectHierarchyConstants().GetDICOMLevelStudy(), slicer.vtkMRMLSubjectHierarchyConstants().GetSubjectHierarchyLevelFolder()]
    self.ui.OutputFolderComboBox.setNodeTypes(nodeTypes)
    self.ui.OutputFolderComboBox.setLevelFilter(levelFilters)
    
    self.ui.InputSegmentationComboBox.connect("currentNodeChanged(vtkMRMLNode*)", self.onSegmentationChanged)
    self.ui.OutputFolderComboBox.connect("currentItemChanged(vtkIdType)", self.onOutputFolderChanged)
    self.ui.NewFolderButton.clicked.connect(self.clearSelection)
    self.ui.SmoothingCheckBox.toggled.connect(self.updateExport)
    self.ui.SmoothingFactorSlider.valueChanged.connect(self.updateExportOnSmoothingSlider)
    self.ui.ResampleCheckBox.toggled.connect(self.updateExport)
    self.ui.ResampleSpinBox.valueChanged.connect(self.updateExportOnResampleSpinBox)

    
    self.ui.SubjectHierarchyTreeView.setShowRootItem(True)
    self.ui.SubjectHierarchyTreeViewOutput.setShowRootItem(True)
    
    self.ui.ExportButton.clicked.connect(self.export)
  
  def clearSelection(self, unused):
    self.ui.OutputFolderComboBox.clearSelection()
    self.ui.OutputFolderComboBox.setCurrentItem(-1)
    self.ui.OutputFolderComboBox.defaultText = 'Export models to new folder'
  
  def onSegmentationChanged(self, node):
    if node is None:
      return
    shNode = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)
    self.ui.SubjectHierarchyTreeView.setMRMLScene(slicer.mrmlScene)
    node_item = shNode.GetItemByDataNode(node)
    self.ui.SubjectHierarchyTreeView.setRootItem(node_item)
    self.ui.SubjectHierarchyTreeView.expandItem(node_item)
    self.initialExportDone = False

    # update voxel spacing value

    masterVolumeNode = self.getMasterVolumeForSegmentation(node)
      

    self.ui.ResampleSpinBox.value = min(masterVolumeNode.GetSpacing())

  def getMasterVolumeForSegmentation(self, node):
    masterVolumeNode = node.GetNodeReference(node.GetReferenceImageGeometryReferenceRole())
    if not masterVolumeNode:
      print('Generating master volume')
      allSegmentsLabelmapNode = slicer.vtkMRMLLabelMapVolumeNode()
      allSegmentsLabelmapNode.SetName(node.GetName() + ' master volume')
      allSegmentsLabelmapNode.HideFromEditorsOn()
      slicer.mrmlScene.AddNode(allSegmentsLabelmapNode)
      result = slicer.vtkSlicerSegmentationsModuleLogic.ExportAllSegmentsToLabelmapNode(node, allSegmentsLabelmapNode)
      node.SetReferenceImageGeometryParameterFromVolumeNode(allSegmentsLabelmapNode)
      segmentationGeometryLogic = slicer.vtkSlicerSegmentationGeometryLogic()
      segmentationGeometryLogic.SetInputSegmentationNode(node)
      segmentationGeometryLogic.SetSourceGeometryNode(allSegmentsLabelmapNode)
      segmentationGeometryLogic.CalculateOutputGeometry()
      segmentationGeometryLogic.ResampleLabelmapsInSegmentationNode()
      masterVolumeNode = node.GetNodeReference(node.GetReferenceImageGeometryReferenceRole())
    return masterVolumeNode

  
  def onOutputFolderChanged(self, item):
    self.initialExportDone = False

  def updateExportOnSmoothingSlider(self, unused):
    if self.ui.SmoothingCheckBox.isChecked():
      self.updateExport()

  def updateExportOnResampleSpinBox(self, unused):
    if self.ui.ResampleCheckBox.isChecked():
      self.updateExport()
  
  def updateExport(self, unused=None):
    if not self.initialExportDone:
      return    
    self.export()
  
  def setParameterUIEnabled(self, enabled = True):
    self.ui.SmoothingCheckBox.enabled = enabled
    self.ui.SmoothingFactorSlider.enabled = enabled
    self.ui.ResampleCheckBox.enabled = enabled
    self.ui.ResampleSpinBox.enabled = enabled
    slicer.app.processEvents()
  
  def export(self):    
    self.setParameterUIEnabled(False)

    shNode = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(slicer.mrmlScene)
    inputSegmentationNode = self.ui.InputSegmentationComboBox.currentNode()
    folderItemID = self.ui.OutputFolderComboBox.currentItem()
    node_item = shNode.GetItemByDataNode(inputSegmentationNode)

    

    if inputSegmentationNode is None:
      return

    # TODO: Clone segmentation node, hide from editors
    itemIDToClone = shNode.GetItemByDataNode(inputSegmentationNode)
    clonedItemID = slicer.modules.subjecthierarchy.logic().CloneSubjectHierarchyItem(shNode, itemIDToClone)
    clonedSegmentationNode = shNode.GetItemDataNode(clonedItemID)
    clonedSegmentationNode.SetName('SEGMENTATION_CLONE')

    # Clone master volume
    masterVolumeNode = self.getMasterVolumeForSegmentation(inputSegmentationNode)
    volumesLogic = slicer.modules.volumes.logic()
    clonedMasterVolumeNode = volumesLogic.CloneVolume(masterVolumeNode, 'MASTER_CLONE')

    # itemIDToClone = shNode.GetItemByDataNode(masterVolumeNode)
    # clonedItemID = slicer.modules.subjecthierarchy.logic().CloneSubjectHierarchyItem(shNode, itemIDToClone)
    # clonedMasterVolumeNode = shNode.GetItemDataNode(clonedItemID)
    # clonedMasterVolumeNode.SetName('MASTER_CLONE')


    # TODO: Set spacing on segmentation node to match input
    if self.ui.ResampleCheckBox.isChecked():
      clonedMasterVolumeNode.SetSpacing(self.ui.ResampleSpinBox.value,self.ui.ResampleSpinBox.value,self.ui.ResampleSpinBox.value)

    clonedSegmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(clonedMasterVolumeNode)
    segmentationGeometryLogic = slicer.vtkSlicerSegmentationGeometryLogic()
    segmentationGeometryLogic.SetInputSegmentationNode(clonedSegmentationNode)
    segmentationGeometryLogic.SetSourceGeometryNode(clonedMasterVolumeNode)
    segmentationGeometryLogic.CalculateOutputGeometry()
    segmentationGeometryLogic.ResampleLabelmapsInSegmentationNode()
    
    #Create folder if needed
    if folderItemID == shNode.GetInvalidItemID() or folderItemID == shNode.GetSceneItemID():
      folderName = inputSegmentationNode.GetName() + '-models'
      folderItemID = shNode.CreateFolderItem( shNode.GetItemParent(node_item), 
        shNode.GenerateUniqueItemName(folderName) )
      self.ui.OutputFolderComboBox.setCurrentItem(folderItemID)

    #Clear folder if needed
    segmentNames = []
    segmentation = clonedSegmentationNode.GetSegmentation()

    
    segmentIDs = vtk.vtkStringArray()
    segmentation.GetSegmentIDs(segmentIDs)
    for IDIndex in range(0, segmentIDs.GetNumberOfValues()):
      ID = segmentIDs.GetValue(IDIndex)
      segment = segmentation.GetSegment(ID)
      segmentNames.append(segment.GetName())

    folderChildren = vtk.vtkIdList()
    shNode.GetItemChildren(folderItemID, folderChildren)

    for childIDIndex in range(0, folderChildren.GetNumberOfIds()):
      childID = folderChildren.GetId(childIDIndex)
      node = shNode.GetItemDataNode(childID)
      if node is None:
        continue
      if node.IsA('vtkMRMLModelNode'):
        if node.GetName() in segmentNames:
          slicer.mrmlScene.RemoveNode(node)

    self.ui.ExportButton.text = 'Exporting...'  
    self.ui.ExportButton.enabled = False
    oldSmoothingFactor = segmentation.GetConversionParameter(slicer.vtkBinaryLabelmapToClosedSurfaceConversionRule().GetSmoothingFactorParameterName())
    # Remove existing representation
    segmentation.RemoveRepresentation(slicer.vtkSegmentationConverter().GetClosedSurfaceRepresentationName())
    if self.ui.SmoothingCheckBox.isChecked():
      smoothingFactor = self.ui.SmoothingFactorSlider.value
      
    else:
      smoothingFactor = -1 * self.ui.SmoothingFactorSlider.value
    segmentation.SetConversionParameter(slicer.vtkBinaryLabelmapToClosedSurfaceConversionRule().GetSmoothingFactorParameterName(), str(smoothingFactor))
    slicer.vtkSlicerSegmentationsModuleLogic.ExportVisibleSegmentsToModels(clonedSegmentationNode, folderItemID)
    segmentation.SetConversionParameter(slicer.vtkBinaryLabelmapToClosedSurfaceConversionRule().GetSmoothingFactorParameterName(), oldSmoothingFactor)
    self.ui.ExportButton.text = 'Export Visible Segments to Surface Models'  
    self.ui.ExportButton.enabled = True

    self.ui.SubjectHierarchyTreeViewOutput.setMRMLScene(slicer.mrmlScene)
    self.ui.SubjectHierarchyTreeViewOutput.setRootItem(folderItemID)
    self.ui.SubjectHierarchyTreeViewOutput.expandItem(folderItemID)
    self.initialExportDone = True

    # Clear 3D representation
    segmentation.RemoveRepresentation(slicer.vtkSegmentationConverter().GetClosedSurfaceRepresentationName())

    # TODO: Remove cloned segmentation node
    slicer.mrmlScene.RemoveNode(clonedSegmentationNode)
    slicer.mrmlScene.RemoveNode(clonedMasterVolumeNode)

    self.setParameterUIEnabled(True)

  
  def onTabChanged(self, tabIndex):
    if tabIndex == self.CurrentIndex:
      return

    #Exit Previous Tab
    if self.CurrentIndex == self.EditorTabIndex:
      self.segWidget.exit()

    #Enter New Tab
    if tabIndex == self.EditorTabIndex:
      self.segWidget.enter()  

    if tabIndex == self.MeshingTabIndex:
      self.meshWidget.enter()  

    #Update Current Tab
    self.CurrentIndex = tabIndex
  
  def setupDICOMBrowser(self):
    #Make sure that the DICOM widget exists
    slicer.modules.dicom.widgetRepresentation()
    self.dicomButton.clicked.connect(self.toggleDICOMBrowser)
    
    #For some reason, the browser is instantiated as not hidden. Close
    #so that the 'isHidden' check works as required
    slicer.modules.DICOMWidget.browserWidget.close()

  def toggleDICOMBrowser(self):
    if slicer.modules.DICOMWidget.browserWidget.isHidden():
        slicer.modules.DICOMWidget.onOpenBrowserWidget()
    else:
      slicer.modules.DICOMWidget.browserWidget.close()
  
  def applyStyle(self):
    # Style
    stylesheetfile = self.resourcePath('Home.qss')
    with open(stylesheetfile,"r") as fh:
      slicer.app.styleSheet = fh.read()

    # central = slicer.util.findChild(slicer.util.mainWindow(), name='CentralWidget')
    # central.setStyleSheet("background-color: #464449")  
        
  def modifyWindowUI(self):
    slicer.util.setModuleHelpSectionVisible(False)
    slicer.util.setApplicationLogoVisible(False)

    slicer.util.setToolbarsVisible(True)
    keepToolbars = [
      slicer.util.findChild(slicer.util.mainWindow(), 'MainToolBar'),
      slicer.util.findChild(slicer.util.mainWindow(), 'ViewToolBar'),
      slicer.util.findChild(slicer.util.mainWindow(), 'ModuleSelectorToolBar'),
      slicer.util.findChild(slicer.util.mainWindow(), 'MouseModeToolBar')
      ]
    slicer.util.setToolbarsVisible(False, keepToolbars)  

#
# HomeLogic
#

class HomeLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget.
  Uses ScriptedLoadableModuleLogic base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def run(self):
    pass  
  
  
class HomeTest(ScriptedLoadableModuleTest):
  """
  This is the test case for your scripted module.
  Uses ScriptedLoadableModuleTest base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setUp(self):
    """ Do whatever is needed to reset the state - typically a scene clear will be enough.
    """
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_Home1()

  def test_Home1(self):
    """ Ideally you should have several levels of tests.  At the lowest level
    tests should exercise the functionality of the logic with different inputs
    (both valid and invalid).  At higher levels your tests should emulate the
    way the user would interact with your code and confirm that it still works
    the way you intended.
    One of the most important features of the tests is that it should alert other
    developers when their changes will have an impact on the behavior of your
    module.  For example, if a developer removes a feature that you depend on,
    your test should break so they know that the feature is needed.
    """

    self.delayDisplay("Starting the test")
    #
    # first, get some data
    #
    
    logic = HomeLogic()
    self.delayDisplay('Test passed!')


#
# Class for avoiding python error that is caused by the method SegmentEditor::setup
# http://issues.slicer.org/view.php?id=3871
#
class HomeFileWriter(object):
  def __init__(self, parent):
    pass
