AevaSlicer by Kitware, Inc.
================================

Customized version of Slicer

![AevaSlicer by Kitware, Inc.](Applications/AevaSlicerApp/Resources/Images/LogoFull.png?raw=true)

[Build Instructions](BUILD.md)

